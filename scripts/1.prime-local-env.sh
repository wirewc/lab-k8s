sudo apt-get update

sudo apt-get install vim -y
sudo apt-get install unzip -y
sudo apt-get install awscli -y

sudo apt-get install python -y
sudo apt-get install python-boto -y
sudo apt-get install python-botocore -y
sudo apt-get install python-boto3 -y
sudo apt-get install python-pip -y

cp ~/lab-k8s/ansible/vars/aws_vars.yaml ~/
