cd ~/
git clone https://github.com/kubernetes-incubator/kubespray.git
cd ~/kubespray
git checkout tags/v2.10.0
sudo pip install -r requirements.txt
# https://github.com/kubernetes-sigs/kubespray/issues/4778#issuecomment-493495780
sudo pip install 'ansible==2.7.9'