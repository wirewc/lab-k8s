cd ~/lab-k8s/
ansible-playbook ansible/playbooks/provision-sshkey.yaml
ansible-playbook ansible/playbooks/prepare-terraform.yaml
cd ~/kubespray/contrib/terraform/aws
~/terraform init
~/terraform apply -auto-approve -var-file="credentials.tfvars"
