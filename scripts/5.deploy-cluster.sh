echo "[defaults]" > ~/.ansible.cfg
echo "host_key_checking = False" >> ~/.ansible.cfg
cd ~/kubespray

ansible-playbook cluster.yml -i inventory/hosts -e ansible_user=core -b --become-user=root --flush-cache
cp ~/kubespray/ssh-bastion.conf ~/ssh-bastion.conf

ansible-playbook ~/lab-k8s/ansible/playbooks/provision-efs.yaml
ansible-playbook ~/lab-k8s/ansible/playbooks/provision-cluster-storage.yaml -i inventory/hosts -e ansible_user=core --ssh-extra-args="-F /home/tbock/ssh-bastion.conf"
ansible-playbook ~/lab-k8s/ansible/playbooks/install-helm.yaml -i inventory/hosts -e ansible_user=core --ssh-extra-args="-F /home/tbock/ssh-bastion.conf"
ansible-playbook ~/lab-k8s/ansible/playbooks/deploy-elk.yaml -i inventory/hosts -e ansible_user=core --ssh-extra-args="-F /home/tbock/ssh-bastion.conf"

ansible-playbook ~/lab-k8s/ansible/playbooks/report-cluster-summary.yaml
